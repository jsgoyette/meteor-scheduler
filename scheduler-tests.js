var wait = Meteor.wrapAsync((delay, cb) => {
  Meteor.setTimeout(() => {
    cb && cb(null, true);
  }, delay);
});

Tinytest.add('Job Queue Handling - queue entry', (test) => {

  Scheduler._entries = {};

  Scheduler.add({
    name: 'Test Job',
    schedule(parser) {
      return null;
    },
    job(done) {
      var result = wait(1200);
      done();
    }
  });

  Scheduler.run();

  var queueEntry = Scheduler.queue.findOne({}, {sort: { entered: 1 }});
  test.equal(queueEntry.status, 'registered');

  Scheduler.queueJob('Test Job');

  wait(1000);

  queueEntry = Scheduler.queue.findOne({}, {sort: { entered: -1 }});
  test.equal(queueEntry.status, 'queued');

  Scheduler.run();

  wait(1000);

  queueEntry = Scheduler.queue.findOne({}, {sort: { entered: -1 }});
  test.equal(queueEntry.status, 'running');

  wait(1000);

  queueEntry = Scheduler.queue.findOne({}, {sort: { entered: -1 }});
  test.equal(queueEntry.status, 'complete');

});

Tinytest.add('Job Queue Handling - clear queue', (test) => {

  Scheduler._entries = {};

  Scheduler.add({
    name: 'Test Job',
    schedule(parser) {
      return null;
    },
    job(done) {
      var result = wait(3000);
      done();
    }
  });

  Scheduler.run();

  Scheduler.queueJob('Test Job');
  Scheduler.clearQueue('Nonexistent Job');

  var numberQueued = Scheduler.queue.find({status: 'queued'}).count();
  test.equal(numberQueued, 1);

  Scheduler.clearQueue('Test Job');

  numberQueued = Scheduler.queue.find({status: 'queued'}).count();
  test.equal(numberQueued, 0);

});

Tinytest.add('Job Queue Handling - queue job in the future', (test) => {

  Scheduler._entries = {};

  Scheduler.add({
    name: 'Test Job',
    schedule(parser) {
      return null;
    },
    job(done) {
      var result = wait(1200);
      done();
    }
  });

  Scheduler.run();

  var queueEntry = Scheduler.queue.findOne({}, {sort: { entered: 1 }});
  test.equal(queueEntry.status, 'registered');

  Scheduler.queueJob('Test Job', {
    schedule: moment().add(6, 'seconds').unix()
  });

  wait(2000);

  queueEntry = Scheduler.queue.findOne({}, {sort: { schedule: -1 }});
  test.equal(queueEntry.status, 'queued');

  Scheduler.run();
  wait(4000);

  queueEntry = Scheduler.queue.findOne({}, {sort: { schedule: -1 }});
  test.equal(queueEntry.status, 'queued');

  wait(1000);
  Scheduler.run();
  wait(1000);

  queueEntry = Scheduler.queue.findOne({}, {sort: { schedule: -1 }});
  test.equal(queueEntry.status, 'running');

});

Tinytest.add('Scheduler Entries - update job', (test) => {

  Scheduler._entries = {};

  Scheduler.add({
    name: 'Test Job',
    schedule(parser) {
      return null;
    },
    job(done) {
      var result = wait(1200);
      done();
    }
  });

  test.isNull(Scheduler.nextScheduledAtDate('Test Job'));

  Scheduler.update('Test Job', {
    schedule(parser) {
      return parser.cron('0 0 * * 0');
    }
  })

  test.isNotNull(Scheduler.nextScheduledAtDate('Test Job'));

});
