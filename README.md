# Scheduler

Scheduler is a server-side only package for managing queued jobs and scheduled tasks. It is intended to be used for both tasks run on a schedule, such as database cleanse jobs or daily reports, as well as one-off background jobs.

## Create New Task

Whether you are creating a scheduled tasks or just sending a job to the queue, the job first needs to be registered with Scheduler. This is done with `Scheduler.add()`.

```js
Scheduler.add({
  name: 'Test Job',
  schedule(parser) {
    return parser.text('every 10 minutes');
  },
  job(done) {
    console.log('running job');
    done(); // <-- important!
  },
});
```
 - name: String
 - schedule: Function
 - job: Function
 - data: Any (optional)
 - retries: Number (optional)

Schedule times are defined using the parser from the [later.js node library](http://bunkat.github.io/later/), which accepts both plain English and cron syntax.

If you wish only to register the job without having it run on a schedule, just return null from the schedule function.

```js
  schedule() {
    return null;
  },
```

The job function takes the callback function `done` as the single argument, which must be called once the job is complete. When the job is run the function is called using `wrapSync`, which requires that the callback be called. As a side effect, if `done` is not called the job will get stuck as running.

Throwing an error will cause the job to fail. When this happens, the entry in the queue log is marked as "failed" and the error is captured.

```js
  job(done) {
    // code here
    if (!fieldValue) {
      throw new Error('fieldValue should not be empty');
    }
    // more code
    done();
  }
```

## Run Scheduler

The Scheduler is started with the `run` method, which will add jobs to the queue that are ready to run (according to their schedule), and then start processing the queue.

```js
Scheduler.run();
```

If you want to run the scheduler every two minutes, for instance, then one solution would be to set up a route that calls the run method and use cron to hit the endpoint every other minute.

```js
Router.route('/cron', () => {

  var host = this.request.headers.host;
  if (host.match(/^127\.0\.0\.1|^localhost/)) {
    Scheduler.run();
  }

  res.end(null);

}, {
  where: 'server'
});

```

```crontab
*/2 * * * * curl http://127.0.0.1:3000/cron
```

## Manually Queueing Tasks

A registered task can be added to the queue using `queueJob`. This can be used to queue a job on some event on the server, such as a save event.

```js
// Scheduler.queueJob(jobName, opts);
Scheduler.queueJob('Test Job');
```

Unlike when running `run`, `queueJob` adds the task to the queue regardless if it is already queued. This can be useful for pushing a batch of small tasks to the queue, all getting different data passed to them.

## Passing Data to Task

Data can be passed to a job function both within `add` and `queueJob`. The passed data is available to the job function as `this.data`. If data is passed to `add`, it is passed to the job function each time the scheduler adds the task to the queue. The data must be json and thus cannot contain functions.

```js
Scheduler.add({
  name: 'Test Data Job',
  schedule(parser) {
    return parser.cron('*/12 * * * *');
  },
  job(done) {
    console.log(this.data.year);
    done();
  },
  data: {
    year: 1987
  }
});
```

If data is passed to `queueJob` it will override the default data defined by `add`.

```js
Scheduler.queueJob('Test Data Job', { data: { year: 1976 }});

// with retries
Scheduler.queueJob('Test Data Job', {
  data: { year: 1976 },
  retries: 2
});
```

## Other Methods

Clear the queue
```js
// cancel all queued jobs
Scheduler.clearQueue();

// cancel all queued jobs by name
Scheduler.clearQueue('Test Data Job');
```

## Options

The only scheduler options currently available are the following. They can be updated directly.

#### Change before startup
```js
Scheduler.options.disablePruneTask = false;
```

#### Change before or after startup
```js
Scheduler.options.maxConcurrent = 3;
Scheduler.options.defaultRetries = 0;
Scheduler.options.pruneOlderThanDays = 14;
```
