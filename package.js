Package.describe({
  name: 'scheduler',
  version: '0.3.2',
  summary: 'Manage scheduled tasks with a job queue',
  git: 'https://bitbucket.org/jsgoyette/meteor-scheduler.git',
  documentation: 'README.md'
});

Npm.depends({
  later: '1.2.0'
});

Package.onUse(function(api) {

  api.versionsFrom('1.2');

  api.use([
    'check',
    'mongo',
    'underscore',
    'ecmascript',
    'momentjs:moment',
    'jsgoyette:migrations@0.2.2',
  ], 'server');

  api.addFiles([
    'scheduler.js',
    'task-prune.js',
    'migrations.js',
  ], 'server');

  api.export([
    'Scheduler',
    'Later'
  ], 'server');

});

Package.onTest(function(api) {
  api.use([
    'tinytest',
    'ecmascript',
    'momentjs:moment',
    'scheduler'
  ]);
  api.addFiles('scheduler-tests.js', 'server');
});
