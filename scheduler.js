/*
TODO
1) Add method - Scheduler.kill
*/

Later = Npm.require('later');

function isDisabled() {
  return !! process.env.DISABLE_SCHEDULER;
}

Scheduler = {

  // list of registered jobs
  _entries: {},

  options: {

    // set before startup
    disablePruneTask: false,

    // set before or after startup
    maxConcurrent: 3,
    defaultRetries: 0,
    pruneOlderThanDays: 14,
  },

  // mongo collection
  queue: new Mongo.Collection('jobQueue'),

  // state
  _processing: false,
  _processingQueue: false,
};

Meteor.startup(() => {
  // always utc
  Later.date.UTC();

  if (isDisabled()) return;

  // if a job was running on restart, set it back to queued
  Scheduler.queue.update({ status: 'running' }, { $set: { status: 'queued' }});

});

/**
 * Scheduler.add
 * register a new scheduled task
 */
Scheduler.add = function(entry) {
  check(entry.name, String);
  check(entry.schedule, Function);
  check(entry.job, Function);

  if (this._entries[entry.name]) {
    throw new Error(`Job already exists with that name (${entry.name}).`);
  }

  this._entries[entry.name] = entry;
};

/**
 * Scheduler.update
 * Update registered scheduled task
 */
 Scheduler.update = function(entryName, entry) {
   check(entryName, String);
   check(entry, Object);

   if (! this._entries[entryName]) {
     throw new Error(`No job exists with that name (${entry.name}).`);
   }

   if (typeof entry.schedule === 'function') {
     this._entries[entryName].schedule = entry.schedule;
   }

   if (typeof entry.job === 'function') {
     this._entries[entryName].job = entry.job;
   }
 };

/**
 * Scheduler.remove
 * Remove registered scheduled task
 *
 */
Scheduler.remove = function(entryName) {
  check(entryName, String);

  if (! this._entries[entryName]) {
    throw new Error(`Job not found with that name (${entryName}).`);
  }

  delete this._entries[entryName];
};

/**
 * Scheduler.nextScheduledAtDate
 * get the date that the schedule should next fire
 */
Scheduler.nextScheduledAtDate = function(jobName) {
  const entry = this._entries[jobName];

  if (entry) {
    const schedule = entry.schedule(Later.parse);
    if (schedule) {
      return Later.schedule(schedule).next(1);
    }
  }

  return null;
};

/**
 * Scheduler.prevScheduledAtDate
 * get the date of the last scheduled fire
 */
Scheduler.prevScheduledAtDate = function(jobName) {
  const entry = this._entries[jobName];

  if (entry) {
    const schedule = entry.schedule(Later.parse);
    if (schedule) {
      return Later.schedule(schedule).prev(1);
    }
  }

  return null;
};

/**
 * Scheduler.queueJob
 * add queue entry for scheduled task if registered
 * here no need to check if the job is already queued because we want to be able
 * to add multiple queue entries (with different data, for example)
 * use opts to override queue entry
 */
Scheduler.queueJob = function(jobName, opts = {}) {
  check(jobName, String);
  check(opts, Object);

  const entry = this._entries[jobName];
  if (!entry) return;

  let retries = Scheduler.options.defaultRetries;
  if (Number(entry.retries) !== NaN) {
    retries = entry.retries;
  }

  const now = moment().unix();

  let item = _.extend({
    schedule: now,
    data: entry.data,
    retries: retries
  }, opts);

  // non-optional values
  item.name = jobName;
  item.entered = now;
  item.status = 'queued';
  item.started = null;

  return this.queue.insert(item);

};

/**
 * Scheduler.clearQueue
 * cancel queued items
 * if jobName is empty all queued items are cancelled
 */
Scheduler.clearQueue = function(jobName) {
  if (isDisabled()) return;

  let where = {
    status: 'queued'
  };

  if (jobName) {
    where.name = jobName;
  }

  this.queue.update(where, { $set: {
    status: 'cancelled'
  }});
};


// start scheduler
// adds ready jobs to queue and initiates process queue
Scheduler.run = function() {
  if (isDisabled()) return;

  // do not process if already processing
  // prevents ddos issues if route is trigger, for example
  if (this._processing) return;
  this._processing = true;

  // schedule each job with later.js
  _.each(this._entries, function(entry) {
    queueUpIfReady(entry);
  });

  // run jobs
  processQueue();

  this._processing = false;
};

/**
 * queueUpIfReady
 * place entry into queue if it's time to run the job
 */
function queueUpIfReady(entry) {

  // get last scheduled run time
  const prevDate = Scheduler.prevScheduledAtDate(entry.name);
  const lastScheduledRun = moment(prevDate).unix();

  const now = moment().unix();

  // get actual last run
  const lastRun = Scheduler.queue.findOne({
    name: entry.name,
    schedule: { $lt: now }
  }, {
    sort: { schedule: -1 }
  }) || {};

  // if not found insert "registered" and wait for next round
  // this is to prevent the job from running right off the bat when first added
  if (!lastRun.entered) {
    Scheduler.queue.insert({
      name: entry.name,
      entered: now,
      schedule: now,
      status: 'registered',
    });
  }

  // skip if already queued or running
  else if (lastRun.status == 'queued' || lastRun.status == 'running') {
    return;
  }

  // insert into queue if last scheduled time is greater than last run time
  else if (lastScheduledRun >= lastRun.schedule) {
    Scheduler.queueJob(entry.name);
  }

};

/**
 * runJob
 * runs job function with data applied to context
 * job is a funtion that gets wrapped with wrapAsync. The job function should
 * take a callback as argument and call the callback when the job is done.
 */
function runJob(job, data) {

  // ensure that data is an object
  data = _.isObject(data) ? data : {};

  // run job in fiber
  const syncJob = Meteor.wrapAsync(job);
  return syncJob.call({ data });

};

/**
 * runQueuedJob
 * process queued job and set to run
 * re-init processQueue if jobs remaining
 */
function runQueuedJob(queuedJob) {

  const jobId = { _id: queuedJob._id };
  const entry = Scheduler._entries[queuedJob.name];

  // don't run if entry not defined
  if (! entry) {
    return Scheduler.queue.update(jobId, {
      $set: {
        status: 'failed',
        error: 'no registered job found'
      }
    });
  }

  // use setTimeout to run jobs concurrently
  Meteor.setTimeout(() => {

    // set to running
    Scheduler.queue.update(jobId, {
      $set: {
        status: 'running',
        started: moment().unix(),
      }
    });

    // catch any exceptions so we can mark as failed
    try {

      // run job
      const res = runJob(entry.job, queuedJob.data);

      // mark as complete
      Scheduler.queue.update(jobId, {
        $set: {
          status: 'complete',
          completed: moment().unix()
        }
      });
    }
    catch(e) {
      console.log(`Scheduler exception [${queuedJob.name}] ${e.stack || e.message}`);

      if (queuedJob.retries > 0) {

        // if retries, catch error, reduce retries and requeue
        Scheduler.queue.update(jobId, {
          $set: {
            status: 'queued',
            retries: queuedJob.retries - 1,
            error: 'exception: ' + e.message,
          }
        });

        // return so not to kick off reprocessing of queue
        return;
      }
      else {
        
        // mark as failed
        Scheduler.queue.update(jobId, { 
          $set: {
            status: 'failed',
            error: e.stack || e.message
          }
        });
      }
    }

    // check if there are more jobs to run
    const remainingQueued = Scheduler.queue.find({ status: 'queued' }).count();

    // if so run process again
    if (remainingQueued) {
      processQueue();
    }
  });

};

/**
 * processQueue
 * send the items in the queue to run
 */
function processQueue() {

  // do not process if already processing
  if (Scheduler._processingQueue) return;
  Scheduler._processingQueue = true;

  // get number of running jobs
  const running = Scheduler.queue.find({status: 'running'}).count();

  // how many to run
  const numberToRun = Scheduler.options.maxConcurrent - running;

  // if not already running max concurrent
  if (numberToRun > 0) {

    // grab queued jobs
    const queuedJobs = Scheduler.queue.find({
      status: 'queued',
      schedule: {
        $lt: moment().unix()
      }
    }, {
      sort: { schedule: 1 },
      limit: numberToRun
    });

    // run them
    queuedJobs.forEach(runQueuedJob);

  }

  Scheduler._processingQueue = false;

};
