/**
 * Prune Job Queue
 * Default task to cleanse the job queue of old records
 * Use Scheduler.options.pruneOlderThanDays to adjust how old the records
 * to be deleted should be
 */
Meteor.startup(() => {

  if (! Scheduler.options.disablePruneTask) {

    Scheduler.add({

      name: "Prune Job Queue",

      schedule(parser) {
        return parser.cron('33 10 * * 0')
      },

      job(done) {

        // cleanup queue
        var days = Scheduler.options.pruneOlderThanDays;

        Scheduler.queue.remove({
          status: { $ne: 'registered' },
          entered: { $lt: moment().subtract(days, 'days').unix() }
        });

        done();
      },

    });
  }
})
