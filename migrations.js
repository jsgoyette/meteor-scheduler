Migrations.registerPackage('scheduler', '0.1.5');

// migration from 0.1.4 to 0.1.5
Migrations.add('scheduler', '0.1.4', '0.1.5', () => {

  // ensure that entries have "schedule" timestamp set
  // to prevent jobs from getting skipped
  Scheduler.queue.find({
    schedule: { $exists: false }
  }).forEach(doc => {
    Scheduler.queue.update({ _id: doc._id }, {
      $set: {
        schedule: doc.entered
      }
    });
  });
});
